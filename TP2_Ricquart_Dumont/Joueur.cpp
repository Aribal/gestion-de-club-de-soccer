#include "Joueur.h"
#include <iostream>

;using namespace std;

Joueur::Joueur()
{
	taille=poids=0;
	villeNaissance = "";
}

Joueur::Joueur(string nnom, string pprenom, float ttaille, float ppoids, string vvilleNaissance)
{
	setNom(nnom);
	setPrenom(pprenom);
	taille = ttaille;
	poids = ppoids;
	villeNaissance = vvilleNaissance;
}

Joueur::~Joueur(){}

float Joueur::getTaille()
{
	return taille;
}

float Joueur::getPoids()
{
	return poids;
}

string Joueur::getVilleNaissance()
{
	return villeNaissance;
}


void Joueur::setTaille(float ttaille)
{
	taille = ttaille;
}

void Joueur::setPoids(float ppoids)
{
	poids = ppoids;
}

void Joueur::setVilleNaissance(string vvillenNaissance)
{
	villeNaissance = vvillenNaissance;
}

void Joueur::afficher()
{
	Sportif::afficher();
	cout<<endl;
	cout<<"Joueur"<<endl<<"Taille : "<<taille<<endl<<"Poids : "<<poids<<endl<<"Ville de naissance : "<<villeNaissance<<endl;
}

void Joueur::ajouterParcours(Parcours &pparcours)
{
	parcoursDuJoueur.push_back(&pparcours);
}

void Joueur::creationJoueur()
{
	
	string chaine;
	cout<<"Creation d'un joueur";
	cout<<"Nom ? ->";cin>>chaine;
	setNom(chaine);
	cout<<"Prenom ? ->";cin>>chaine;
	setPrenom(chaine);
	cout<<"Taille ? -> "; cin>>taille;
	cout<<"Poids ? -> "; cin>>poids;
	cout<<"Ville de naissance ? -> "; cin>>chaine;
	setVilleNaissance(chaine);

}

