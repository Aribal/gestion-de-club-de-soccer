#include "Contrat.h"

using namespace std;

Contrat::Contrat()
{

}

Contrat::Contrat(Joueur jjoueur, Club cclubContractant, Club cclubLibere, int ddureeContrat, Date ddateEntree, 
		Reglement rreglement, Date ddate)
{
	joueur = jjoueur;
	clubContractant= cclubContractant;
	clubLibere = cclubLibere;
	dateEntree = ddateEntree;
	date = ddate;
	dureeContrat = ddureeContrat;
	reglement = rreglement;
}

Contrat::~Contrat()
{

}

Joueur Contrat::getJoueur()
{
	return joueur;
}

Club Contrat::getClubContractant()
{
	return clubContractant;
}

Club Contrat::getClubLibere()
{
	return clubLibere;
}

int Contrat::getDureeContrat()
{
	return dureeContrat;
}

Date Contrat::getDateEntree()
{
	return dateEntree;
}

Reglement Contrat::getReglement()
{
	return reglement;
}

Date Contrat::getDate()
{
	return date;
}

void Contrat::setJoueur(Joueur jjoueur)
{
	joueur = jjoueur;
}

void Contrat::setClubContractant(Club cclubContractant)
{
	clubContractant = cclubContractant;
}

void Contrat::setClubLibere(Club cclubLibere)
{
	clubLibere = cclubLibere;
}

void Contrat::setDureeContrat(int ddureeContrat)
{
	dureeContrat = ddureeContrat;
}

void Contrat::setDateEntree(Date ddateEntree)
{
	dateEntree = ddateEntree;
}

void Contrat::setReglement(Reglement rreglement)
{
	reglement = rreglement;
}

void Contrat::setDate(Date ddate)
{
	date = ddate;
}

void Contrat::afficher()
{
	cout<<"Joueur : "<<joueur.getNom()<<" "<<joueur.getPrenom()<<endl;
	cout<<"Club contractant : "<<clubContractant.getNom()<<endl;
	cout<<"Club libere : "<<clubLibere.getNom()<<endl;
	cout<<"Duree du contrat : "<<dureeContrat<<endl;
	cout<<"Date d'entree : ";dateEntree.afficher();	
	cout<<"Reglement : \n";reglement.afficher();
	cout<<"Date du contrat : ";date.afficher();
}


