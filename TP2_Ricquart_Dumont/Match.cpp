#include <iostream>
#include <string>
#include "Match.h"


;using namespace std;

Match::Match()
{
	equipeLocale=equipeVisiteur=Equipe();
	resultat=Resultat();
}

Match::Match(Equipe eequipeLocale, Equipe eequipeVisiteur)
{
	equipeLocale=eequipeLocale;
	equipeVisiteur=eequipeVisiteur;
}

Match::Match(Equipe eequipeLocale, Equipe eequipeVisiteur, Resultat rresultat)
{
	equipeLocale=eequipeLocale;
	equipeVisiteur=eequipeVisiteur;
	resultat=rresultat;
}

Match::~Match(){}

Equipe Match::getEquipeLocale()
{
	return equipeLocale;
}

Equipe Match::getEquipeVisiteur()
{
	return equipeVisiteur;
}

Resultat Match:: getResultat()
{
	return resultat;
}

void Match::setEquipeLocale(Equipe eequipeLocale)
{
	equipeLocale=eequipeLocale;
}

void Match::setEquipeVisiteur(Equipe eequipeVisiteur)
{
	equipeVisiteur=eequipeVisiteur;
}

void Match::setResultat(Resultat rresultat)
{
	resultat=rresultat;
}

void Match::afficherMatch()
{
	cout<<equipeLocale.getNomClub()<<" "<<resultat.getNombreButsLocal()<<" - ";
	cout<<resultat.getNombreButsVisiteur()<<" "<<equipeVisiteur.getNomClub()<<endl;
}

	void Match::creerMatch()
{
	cout<<"enregistrement equipe locale"<<endl;
	equipeLocale.creerEquipe();
	cout<<"enregistrement equipe visiteur"<<endl;
	equipeVisiteur.creerEquipe();
	resultat.creerResultat();
	Match match=Match(equipeLocale, equipeVisiteur, resultat);
	//match.afficherMatch();
	
}