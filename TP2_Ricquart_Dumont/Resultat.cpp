#include <iostream>
#include <string>
#include "Resultat.h"


;using namespace std;

Resultat::Resultat()
{
	nombreButsLocal=nombreButsVisiteur=0;
}

Resultat::Resultat(int nnombreButsLocal, int nnombreButsVisiteur)
{
	nombreButsLocal=nnombreButsLocal;
	nombreButsVisiteur=nnombreButsVisiteur;
}

Resultat::~Resultat(){}

int Resultat::getNombreButsLocal()
{
	return nombreButsLocal;
}

int Resultat::getNombreButsVisiteur()
{
	return nombreButsVisiteur;
}

void Resultat::setNombreButsLocal(int nnombreButsLocal)
{
	nombreButsLocal=nnombreButsLocal;
}

void Resultat::setNombreButsVisiteur(int nnombreButsVisiteur)
{
	nombreButsVisiteur=nnombreButsVisiteur;
}

void Resultat::afficherResultat()
{
	cout<<"score equipe locale: "<<nombreButsLocal<<endl;
	cout<<"score equipe visiteur: "<<nombreButsVisiteur;
}

void Resultat::creerResultat()
{
	Resultat res;
	cout<<"nombre buts marques par l'equipe locale?\n";
	cin>>nombreButsLocal;
	cout<<"nombre buts marques par l'equipe visiteur?\n";
	cin>>nombreButsVisiteur;
	cout<<endl;
	res=Resultat(nombreButsLocal, nombreButsVisiteur);
}
