#ifndef MENUCLUB_H
#define MENUCLUB_H

#include"Menu.h"
#include"MenuPrincipal.h"
#include "Club.h"
;using namespace std;

class MenuClub : public Menu
{
public:
	~MenuClub();

	void afficher();
	void effectuerChoix(int choix);

	Club creationClub();
	void ajoutClub();

};


#endif // !MENUCLUB_H
