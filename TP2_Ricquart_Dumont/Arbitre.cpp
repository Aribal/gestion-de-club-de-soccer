#include "arbitre.h"
;using namespace std;

Arbitre::Arbitre()
{
	lieuObtentionGrade = "";
	expArbitrage = 0;
}

Arbitre::Arbitre(string nnom, string pprenom, string llieuObtentionGrade, int eexpArbitrage)
{
	setNom(nnom);
	setPrenom(pprenom);
	lieuObtentionGrade = llieuObtentionGrade;
	expArbitrage = eexpArbitrage;
}

Arbitre::~Arbitre(){}

string Arbitre::getLieuObtentionGrade()
{
	return lieuObtentionGrade;
}
int Arbitre::getExpArbitrage()
{
	return expArbitrage;
}

void Arbitre::setLieuObtentionGrade(string llieuObtentionGrade)
{
	lieuObtentionGrade = llieuObtentionGrade;
}

void Arbitre::setExpArbitrage(int eexpArbitrage)
{
	expArbitrage = eexpArbitrage;
}

void Arbitre::afficher()
{
	Sportif::afficher();
	cout<<"Arbitre"<<endl<<"Lieu d obtention du grade : "<<lieuObtentionGrade<<endl<<"Experience d arbitrage : "<<expArbitrage<< " ans "<<endl;
}



