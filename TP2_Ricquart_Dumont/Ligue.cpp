#include <iostream>
#include <string>
#include <vector>
#include "Ligue.h"
#include "Club.h"
#include "Contrat.h"

;using namespace std;

Ligue::Ligue()
{
	Date A = Date(1,1,1);
	Date B = Date(2,2,2);
	Club AA = Club("PSG","pouarf","bleu fonce",A, "Paris");
	Club BB = Club("OM", "pfffff", "jaune", B, "Marseille");
	ajoutClub(AA);
	ajoutClub(BB);
}


Ligue::~Ligue(){}

Club Ligue::getClub(int i)
{
	return listeClub.at(i);
}

Club Ligue::getClub()
{
	return listeClub.at(choisirClub());
}

int Ligue::choisirClub()
{
	int i;
	afficherListeClub();
	cout<<"CHOIX : "<<endl;
	cin>>i;
	return i-1;
}
void Ligue::supprimerClub()
{
	int i;
	cout<<"Numero du club a supprimer"<<endl;
	i=choisirClub();
	listeClub.erase(listeClub.begin()+i);
}


void Ligue::ajoutClub()
{
	listeClub.push_back(creationClub());
}
void Ligue::ajoutClub(Club club)
{
	listeClub.push_back(club);
}
Club Ligue::creationClub()
{
	Club club;
	club.creationClub();
	cout<<club.getNom()<<" cree"<<endl;
	return club;
}

void Ligue::ajoutJoueur()
{
	int i = choisirClub();
	Club club = getClub(i);
	vector<Joueur> effectif = club.getEffectif();
	Joueur joueur = creationJoueur();
	
	effectif.push_back(joueur);
	club.setEffectif(effectif);
	listeClub[i]=club;
}
Joueur Ligue::creationJoueur()
{
	Joueur joueur;
	joueur.creationJoueur();
	cout<<joueur.getNom()<<" cree"<<endl;
	return joueur;
}
void Ligue::supprimerJoueur()
{
	int i;
	int j;
	cout<<"supprimer un joueur de quel club ?"<<endl;
	i=choisirClub();
	Club club  = getClub(i);
	club.afficherEffectif();
	cout<<"joueur a supprimer : "<<endl;
	cin>>j;
	club.setEffectif(club.supprimerJoueur(j));
	listeClub[i]=club;
}


void Ligue::afficherListeClub()
{
	cout<<"Affichage des clubs : "<<endl;
	for (unsigned int i = 0; i<listeClub.size();i++)
	{
		cout<<i+1<<") ";
		cout<<listeClub.at(i).getNom()<<endl;
	}
}

void Ligue::transfert()
{
	Contrat contrat;
	Club clubContractant, clubLibere;
	int numeroJoueur,i,j;
	Joueur joueur;
	cout<<"\nSelection du club de depart : "<<endl;
	i=choisirClub();
	clubLibere=getClub(i);
	cout<<"\nSelection du joueur a transferer : "<<endl;
	clubLibere.afficherEffectif();
	cin>>numeroJoueur;
	numeroJoueur-=1;
	joueur = clubLibere.getEffectif().at(numeroJoueur);
	cout<<"\nSelection du club d'arrivee : "<<endl;
	j=choisirClub();
	clubContractant=getClub(j);
	clubContractant.getEffectif().push_back(joueur);
	clubContractant.afficherEffectif();
	clubLibere.supprimerJoueur(i+1);
	listeClub[i] = clubLibere;
	listeClub[j] = clubContractant; 
	
}
