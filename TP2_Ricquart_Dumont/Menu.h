#ifndef MENU_H
#define MENU_H

#include "Club.h"
#include "Ligue.h"
;using namespace std;

class Menu
{
public:
	Menu();
	Menu(Ligue ligue);
	~Menu();

	int getChoix();
	Ligue getLigue();
	void setChoix(int cchoix);

	void afficher();
	int entrerChoix();
	void effectuerChoix(int cchoix);



private:
	int choix;
	Ligue ligue;
};

#endif // !MENU_H
