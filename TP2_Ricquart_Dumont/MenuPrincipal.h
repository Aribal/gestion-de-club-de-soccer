#ifndef MENUPRINCIPAL_H
#define MENUPRINCIPAL_H

#include "Menu.h"
#include "MenuClub.h"
;using namespace std;

class MenuPrincipal : public Menu
{
public:
	MenuPrincipal();
	~MenuPrincipal();

	void afficher();
	void effectuerChoix(int cchoix);
	void creerMenuClub(Ligue ligue);


private:

};

#endif // !MENUPRINCIPAL_H
