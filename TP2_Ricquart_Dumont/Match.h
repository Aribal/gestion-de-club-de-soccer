#ifndef MATCH_H
#define MATCH_H
#include "Resultat.h"
#include "Equipe.h"
#include <string>
;using namespace std;

class Match
{
public:
	Match();
	Match(Equipe eequipeLocale, Equipe eequipeVisiteur, Resultat rresultat);
	Match(Equipe eequipeLocale, Equipe eequipeVisiteur);
	~Match();

	//Getters
	Equipe getEquipeLocale();
	Equipe getEquipeVisiteur();
	Resultat getResultat();

	//Setters
	void setEquipeLocale(Equipe eequipeLocale);
	void setEquipeVisiteur(Equipe eequipeVisiteur);
	void setResultat(Resultat rresultat);

	//M�thodes
	void afficherMatch();
	void creerMatch();



private:
	Equipe equipeLocale, equipeVisiteur;
	Resultat resultat;
};


#endif // !MATCH_H


