#ifndef CALENDRIER_H
#define CALENDRIER_H
#include "Rencontres.h"
#include "Ligue.h"
#include <string>
;using namespace std;

class Calendrier
{
public:
	Calendrier();
	Calendrier(vector<Rencontres> llisteRencontres);
	~Calendrier();

	//Getters
	vector<Rencontres> getListeRencontres();

	//Setters
	void setListeRencontres(vector<Rencontres> llisteRencontres);


	//M�thodes
	void afficherCalendrierComplet();
	void afficherCalendrierPerso();
	void ajoutRencontre();
	void creerCalendrier();
	void supprimerRencontre();
	void afficherCalendrierDate();



private:
	vector<Rencontres> listeRencontres;
};


#endif // !CALENDRIER_H



