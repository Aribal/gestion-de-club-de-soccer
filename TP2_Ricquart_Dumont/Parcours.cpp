#include <string>
#include <iostream>
#include "Parcours.h"

;using namespace std;

Parcours::Parcours()
{
	nomClub = "ecole de foot";
	datePassage = Date(1,1,1);
}

Parcours::Parcours(std::string nnomClub, Date ddatePassage)
{
	nomClub = nnomClub;
	datePassage = ddatePassage;
}

Parcours::~Parcours(){}

string Parcours::getNomClub()
{
	return nomClub;
}

Date Parcours::getDatePassage()
{
	return datePassage;
}

void Parcours::setNomClub(string nnomClub)
{
	nomClub = nnomClub;
}

void Parcours::setDatePassage(Date ddatePassage)
{
	datePassage = ddatePassage;
}

void Parcours::afficher()
{
	cout<<"1 parcours"<<endl;
	cout<<nomClub<<" ";
	datePassage.afficher();
}

void Parcours::creerParcours()
{
	cout<<"\nClub ? -> "; cin>>nomClub;
//	creerDate();
}
