#ifndef CLUB_H
#define CLUB_H

#include <iostream>
#include <string>
#include <vector>

#include "Date.h"
#include "Joueur.h"
//#include "Contrat.h"


;using namespace std;

class Club
{
public :
	Club();
	Club(std::string nnom, std::string hhistoire, std::string ccouleur,Date ddateCreation, std::string vville);
	~Club();

	//GET
	std::string getNom();
	std::string getHistoire();
	std::string getCouleur();
	std::string getVille();
	Date getDateCreation();	
	vector<Joueur> getEffectif();
	//vector<Contrat> getListeContrat();

	//SET
	void setNom(string nnom);
	void setHistoire(string hhistoire);
	void setCouleur(string ccouleur);
	void setVille(string vville);
	void setDateCreation(Date ddateCreation);
	void setEffectif(vector<Joueur> eeffectif);
	//void setListeContrat(vector<Contrat> llisteContrat);
	
	//M�TH
	void afficherClub();
	void afficherEffectif();
	void creationClub();
	vector<Joueur> supprimerJoueur(int i);

private : 
	
	std::string nom, histoire, couleur, ville;
	Date dateCreation;
	//std::vector<Contrat> listeContrat;

protected:
	std::vector<Joueur> effectif;
}
#endif // !CLUB_H