#include <string>
#include <vector>
#include <iostream>
#include "Club.h"



;using namespace std;

Club::Club()
{
	nom = histoire = couleur = ville = "";
}

Club::Club(std::string nnom, std::string hhistoire, std::string ccouleur, Date ddateCreation, std::string vville)
{
	nom = nnom;
	histoire = hhistoire;
	couleur = ccouleur;
	ville = vville;
	dateCreation = ddateCreation;
	Joueur joueurA("A","A",1,1,"A");
	Joueur joueurB("B","B",1,1,"B");
	Joueur joueurC("C","C",1,1,"C");
	Joueur joueurD("D","D",1,1,"D");
	effectif.push_back(joueurD);
	effectif.push_back(joueurC);
	effectif.push_back(joueurB);
	effectif.push_back(joueurA);
}

Club::~Club(){}

string Club::getNom()
{
	return nom;
}
string Club::getHistoire()
{
	return histoire;
}
string Club::getCouleur()
{
	return couleur;
}
string Club::getVille()
{
	return ville;
}
Date Club::getDateCreation()
{
	return dateCreation;
}
vector<Joueur> Club::getEffectif()
{
	return effectif;
}

void Club::setNom(std::string nnom)
{
	nom = nnom;
}
void Club::setHistoire(std::string hhistoire)
{
	histoire = hhistoire;
}
void Club::setCouleur(std::string ccouleur)
{
	couleur = ccouleur;
}
void Club::setVille(std::string vville)
{
	ville = vville;
}
void Club::setDateCreation(Date ddateCreation)
{
	dateCreation = ddateCreation;
}
void Club::setEffectif(vector<Joueur> eeffectif)
{
	effectif = eeffectif;
}

void Club::afficherClub()
{
		
	cout <<"\nAffichage du club\n"<<endl;
	cout<<"nom du club : "<<nom<<endl;
	cout<<"histoire : "<<histoire<<endl;
	cout<<"creation le : ";
	dateCreation.afficher();
	cout<<"couleur : "<<couleur<<endl;
	cout<<"ville : "<<ville<<endl << endl;
	afficherEffectif();
	
}

void Club::afficherEffectif()
{
	for (unsigned int i = 0; i<effectif.size();i++)
	{
		cout<<" "<<i+1<<") "<<effectif[i].getNom()<<endl;
	}
}

void Club::creationClub()
{
	cout<<"\nCreation d'un Club";
	cout<<"\nNom ? -> ";cin>>nom;
	cout<<"Histoire ? -> ";cin>>histoire;
	cout<<"Date de creation ? -> ";
	dateCreation.creerDate();
	cout<<"Couleur ? -> "; cin>>couleur;
	cout<<"Ville ? -> "; cin>>ville;
}

vector<Joueur> Club::supprimerJoueur(int i)
{
	effectif.erase(effectif.begin()+i);
	return effectif;
}