#include "Equipe.h"
#include "Menu.h"
#include "MenuPrincipal.h"
#include "Ecran.h"
#include <iostream>

;using namespace std;

Equipe::Equipe()
{
	Club club=Club();
	nombreGardiens=0;
	nombreJoueurs=0;
	Joueur capitaine=Joueur();
}

Equipe::Equipe(Club cclub, int nnombreJoueurs, int nnombreGardiens, Joueur ccapitaine)
{
	club= cclub;
	nombreJoueurs=nnombreJoueurs;
	nombreGardiens=nnombreGardiens;
	capitaine=ccapitaine;
}

Equipe::~Equipe(){}

Club Equipe::getClub()
{
	return club;
}

string Equipe::getNomClub()
{
	return club.getNom();
}

int Equipe::getNombreJoueurs()
{
	return nombreJoueurs;
}

int Equipe::getNombreGardiens()
{
	return nombreGardiens;
}

Joueur Equipe::getCapitaine()
{
	return capitaine;
}

void Equipe::setClub(Club cclub)
{
	club=cclub;
}

void Equipe::setNombreJoueurs(int nnombreJoueurs)
{
	nombreJoueurs=nnombreJoueurs;
}

void Equipe::setNombreGardiens(int nnombreGardiens)
{
	nombreGardiens=nnombreGardiens;
}

void Equipe::setCapitaine(Joueur ccapitaine)
{
	capitaine=ccapitaine;
}

void Equipe::afficherEquipe()
{
	cout<<"club: ";
	club.afficherClub();
	cout<<endl;
	cout<<"nombre joueurs: "<<nombreJoueurs<<endl;
	cout<<"nombre gardiens: "<<nombreGardiens<<endl;
	cout<<"capitaine: "<<endl;
	capitaine.afficher();
}

	void Equipe::creerEquipe()
{
	Equipe eq;
	Ligue lig;
	int choix;
	cout<<"Selectionner le club dont depend l'equipe"<<endl;
	lig.afficherListeClub();
	cin>>choix;
	choix=choix-1;
	choix=choix;
	club=lig.getClub(choix);
	setClub(club);
	cout<<"nombre de joueurs dans l'equipe?"<<endl;
	cin>>nombreJoueurs;
	setNombreJoueurs(nombreJoueurs);
	cout<<"nombre de gardiens dans l'equipe?"<<endl;
	cin>>nombreGardiens;
	setNombreGardiens(nombreGardiens);
	cout<<"Selectionner le capitaine"<<endl;
	club.afficherEffectif();
	cin>>choix;
	choix=choix-1;
	
	capitaine=club.getEffectif()[choix];
	eq=Equipe(club, nombreJoueurs, nombreGardiens, capitaine);
	//eq.afficherEquipe();


}
