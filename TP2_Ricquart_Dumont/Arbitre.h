#ifndef ARBITRE_H
#define ARBITRE_H

#include"Sportif.h"
;using namespace std;

class Arbitre : public Sportif
{
public:
	Arbitre();
	Arbitre(std::string nnom, std::string pprenom, std::string llieuObtentionGrade, int eexpArbitrage);
	~Arbitre();

	//GET
	std::string getLieuObtentionGrade();
	int getExpArbitrage();

	//SET
	void setLieuObtentionGrade(string llieuObtentionGrade);
	void setExpArbitrage(int eexpArbitrage);

	//METH
	void afficher();

private:
	string lieuObtentionGrade;
	int expArbitrage;
	};
#endif // !ARBITRE_H
