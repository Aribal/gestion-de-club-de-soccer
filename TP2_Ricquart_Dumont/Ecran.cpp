#include <iostream>
#include <vector>
#include "Ecran.h"
#include "MenuCalendrier.h"

using namespace std;


int Ecran::getChoix()
{
	return choix;
}

Ligue Ecran::getLigue()
{
	return ligue;
}
Calendrier Ecran::getCalendrier()
{
	return calendrier;
}

void Ecran::setChoix(int cchoix)
{
	choix = cchoix;
}

void Ecran::setLigue(Ligue lligue)
{
	ligue = lligue;
}
void Ecran::setCalendrier(Calendrier ccalendrier)
{
	calendrier=ccalendrier;
}

int Ecran::entrerChoix()
{
	cout<<"CHOIX :  ";
	cin>>choix;
	setChoix(choix);
	return choix;
}

void Ecran::menuPrincipal()
{
		cout<<"\n\nMENU PRINCIPAL\n"<<endl;
	cout<<"1) Gestion des Club\n2) Gerer les joueurs\n3) Gestion des calendriers\n4) Creation d'un match\n5) Gestion des transferts\n6) Resultats et titres\n8) Quitter"<<endl;
	faireMenuPrincipal(entrerChoix());
}

void Ecran::faireMenuPrincipal(int choix)
{
		switch (choix)
	{
	case 1:
		menuClub();
		break;
	case 2 :
		menuJoueur();
		break;
	case 3 :
		menuCalendrier();
		break;
	case 4 :
		//TODO
		break;
	case 5 :
		menuTransfert();
		break;
	case 6 :
		//TODO
		break;
	case 7 :
		//TODO
		break;
	case 8 :
		//TODO
		cout<<"Fermeture du programme"<<endl;
		break;
	default:
		cout<<"Choix invalide (ou j'ai pas fini)"<<endl;
		faireMenuPrincipal(entrerChoix());
		break;
	}
}

void Ecran::menuClub()
{
	cout<<"\n\nMENU CLUB\n"<<endl;
	cout<<"1) Creer un Club"<<endl;
	cout<<"2) Supprimer un Club"<<endl;
	cout<<"3) Afficher la liste des clubs"<<endl;
	cout<<"4) Retour "<<endl;
	faireMenuClub(entrerChoix());
}

void Ecran::faireMenuClub(int choix)
{
	switch (choix)
	{
	case 1 :
		cout<<"creation club"<<endl;
		ligue.ajoutClub();
		menuClub();
		break;

	case 2 : 
		cout<<"ici suppression"<<endl;
		ligue.supprimerClub();
		menuClub();	
		break;
	case 3 :
		getLigue().afficherListeClub();
		system("pause");
		menuClub();
		break;
	case 4 :
		menuPrincipal();
		break;

	default:
		cout<<"Choix invalide"<<endl;
		faireMenuClub(entrerChoix());
		break;
	}
}

void Ecran::menuJoueur()
{
	cout<<"\n\nMENU JOUEURS\n"<<endl;
	cout<<"1) Creer un joueur"<<endl;
	cout<<"2) Supprimer un Joueur"<<endl;
	cout<<"3) Afficher la liste des joueurs d'un club"<<endl;
	cout<<"4) Retour "<<endl;
	faireMenuJoueur(entrerChoix());
}

void Ecran::faireMenuJoueur(int choix)
{
	switch (choix)
	{
	case 1 :
		ligue.ajoutJoueur();
		menuJoueur();
		break;
	case 2 : 
		ligue.supprimerJoueur();
		menuJoueur();	
		break;
	case 3 :
		ligue.getClub().afficherEffectif();
		system("pause");
		menuJoueur();
		break;
	case 4 :
		menuPrincipal();
		break;
	default:
		cout<<"Choix invalide"<<endl;
		faireMenuJoueur(entrerChoix());
		break;
	}
}

void Ecran::menuCalendrier()
{
	cout<<"\n\nMENU CALENDRIER\n"<<endl;
	
	cout<<"MENU CALENDRIER\n"<<endl;
	cout<<"1/ consulter le calendrier"<<endl;
	cout<<"2/ ajouter des rencontres"<<endl;
	cout<<"3/ supprimer une rencontre"<<endl;
	cout<<"4/ retour\n"<<endl;

	choix=entrerChoix();

	switch (choix)
	{
	case 1:
	cout<<"Voulez-vous afficher \n1/ toutes les rencontres \n2/ les rencontres d'un seul club \n3/ les rencontres d'une date donnee"<<endl;
		cin>>choix;
		choix=choix;
		if(choix==1)
			calendrier.afficherCalendrierComplet();
		else
		{
			if(choix==2)
				calendrier.afficherCalendrierPerso();
			else
			{
				if (choix==3)
					calendrier.afficherCalendrierDate();
				else
				cout<<"erreur"<<endl;
			}
		}
		menuCalendrier();
		break;

		break;


	case 2:
		calendrier.ajoutRencontre();
		menuCalendrier();
		break;

	case 3:
		calendrier.supprimerRencontre();
		menuCalendrier();
		break;

	case 4:
		menuPrincipal();
		break;

	}
}


void Ecran::faireMenuCalendrier(int choix)
{

}

void Ecran::menuTransfert()
{
	cout<<"\n\nMENU TRANSFERTS\n";
	ligue.transfert();
	menuPrincipal();
	setLigue(ligue);
}