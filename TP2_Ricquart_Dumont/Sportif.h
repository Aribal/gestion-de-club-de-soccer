#ifndef SPORTIF_H
#define SPORTIF_H

#include <iostream>
#include <string>

;using namespace std;

class Sportif
{
public :
	//constructeurs destructeurs
	Sportif();
	Sportif(std::string nnom, std::string pprenom);
	~Sportif();

	//GET
	std::string getNom();
	std::string getPrenom();

	//SET
	void setNom(std::string nnom);
	void setPrenom(std::string pprenom);

	//M�THODES
	void afficher();
	void creationSportif();

private:
	std::string nom, prenom;
}

#endif