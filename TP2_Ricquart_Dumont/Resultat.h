#ifndef RESULTAT_H
#define RESULTAT_H

#include <string>
;using namespace std;

class Resultat
{
public:
	Resultat();
	Resultat(int nnombreButsLocal, int nnombreButsVisiteur);
	~Resultat();

	//Getters
	int getNombreButsLocal();
	int getNombreButsVisiteur();
	//Setters
	void setNombreButsLocal(int nnombreButsLocal);
	void setNombreButsVisiteur(int nnombreButsVisiteur);
	//M�thodes
	void afficherResultat();
	void creerResultat();




private:
	int nombreButsLocal, nombreButsVisiteur;
};


#endif // !RESULTAT_H


