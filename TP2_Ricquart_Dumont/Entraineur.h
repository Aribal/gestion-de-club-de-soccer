#ifndef ENTRAINEUR_H
#define ENTRAINEUR_H

#include <vector>
#include "Staff.h"

class Entraineur:public Staff
{

public:

Entraineur();
Entraineur(TitreGagne titresGagnesRecu);
~Entraineur();
TitreGagne getTitresGagnes();
void setTitresGagnes(TitreGagne titresGagnesRecu);

private:


};
#endif