
#ifndef CONTRAT_H
#define CONTRAT_H



#include <iostream>
#include <string>
#include "Club.h"
#include "Reglement.h"

using namespace std;

class Contrat
{
public:
	Contrat();
	Contrat(Joueur jjoueur, Club cclubContractant, Club cclubLibere, int ddureeContrat, Date ddateEntree, Reglement rreglement, Date ddate);
	~Contrat();

	Joueur getJoueur();
	Club getClubContractant();
	Club getClubLibere();
	int getDureeContrat();
	Date getDateEntree();
	Reglement getReglement();
	Date getDate();

	void setJoueur(Joueur jjoueur);
	void setClubContractant(Club cclub);
	void setClubLibere(Club cclub);
	void setDureeContrat(int ddureeContrat);
	void setDateEntree(Date ddateEntree);
	void setReglement(Reglement rreglement);
	void setDate(Date ddate);

	void afficher();
	

private:
	Joueur joueur;
	Club clubContractant, clubLibere;
	int dureeContrat;
	Date dateEntree;
	Reglement reglement;
	Date date;
};


#endif // !CONTRAT_H