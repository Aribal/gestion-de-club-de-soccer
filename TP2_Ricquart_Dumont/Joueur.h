#ifndef JOUEUR_H
#define JOUEUR_H


#include <list>
#include "Sportif.h"
#include "Parcours.h"
;using namespace std;


class Joueur : public Sportif
{
public:

	Joueur();
	Joueur(std::string nnom, std::string pprenom,float ttaille, float ppoids, std::string vvilleNaissance);
	~Joueur();

	float getTaille();
	float getPoids();
	std::string getVilleNaissance();

	void setTaille(float ttaille);
	void setPoids(float ppoids);
	void setVilleNaissance(string vvilleNaissance);

	void afficher();
	void ajouterParcours(Parcours &pparcours);
	void creerParcours();
	void creationJoueur();


private:

	float taille;
	float poids;
	string villeNaissance;
	list<Parcours*> parcoursDuJoueur;
};
#endif // !JOUEUR_H

