#ifndef LIGUE_H
#define LIGUE_H

#include <iostream>
#include <vector>
#include "Club.h"
#include "Contrat.h"


;using namespace std;

class Ligue
{
public:
	Ligue();
	~Ligue();

	Club getClub();
	Club getClub(int i);
	//Calendrier getCalendrier();
	
	void setClub(Club club);
	//void setCalendrier(Calendrier ccalendrier);

	int choisirClub();
	void ajoutClub(Club club);
	void ajoutClub();
	void ajoutJoueur();
	void supprimerJoueur();
	void supprimerClub();
	void afficherListeClub();
	Club creationClub();
	Joueur creationJoueur();
	void transfert();


	

private:
	std::vector<Club> listeClub;
	//Calendrier calendrier;
}

#endif // !LIGUE_H
