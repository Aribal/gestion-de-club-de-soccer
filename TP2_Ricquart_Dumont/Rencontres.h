#ifndef RENCONTRES_H
#define RENCONTRES_H
#include "Resultat.h"
#include "Equipe.h"
#include "Date.h"
#include "Match.h"
#include <string>
;using namespace std;

class Rencontres
{
public:
	Rencontres();
	Rencontres(Date ddateRencontre, Match mmatch);
	~Rencontres();

	//Getters
	Date getDateRencontre();
	Match getMatch();

	//Setters
	void setDateRencontre(Date ddateRencontre);
	void setMatch(Match mmatch);

	//M�thodes
	void afficherRencontre();
	void creerRencontre();



private:
	Date dateRencontre;
	Match match;
};


#endif // !RENCONTRES_H


