#ifndef REGLEMENT_H
#define REGLEMEMT_h

#include <string>

;using namespace std;

class Reglement
{
public:
	Reglement();
	Reglement(float sseuilEnVigueur, float mmontantTranfert, float mmontantEncaisse, float mmontantRestant, string ddescritpionDesDroits);
	~Reglement();

	float getSeuilEnVigueur();
	float getMontantTransfert();
	float getMontantEncaisse();
	float getMontantRestant();
	string getDescriptionDesDroits();

	void setSeuilEnVigueur(float sseuilEnVigueur);
	void setMontantTransfert(float mmontantTransfert);
	void setMontantEncaisse(float mmontantEncaisse);
	void setMontantRestant(float mmontantRestant);
	void setDescriptionDesDroits(string ddescriptionDesDroits);

	void afficher();

private:
	float seuilEnVigueur, montantTransfert, montantEncaisse, montantRestant;
	string descriptionDesDroits;
};

#endif // !REGLEMENT_H
