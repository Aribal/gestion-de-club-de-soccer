#ifndef MENUCALENDRIER_H
#define MENUCALENDRIER_H

#include"Menu.h"
#include"MenuPrincipal.h"
#include "Rencontres.h"
#include "Calendrier.h"
;using namespace std;

class MenuCalendrier
{
public:
	MenuCalendrier();
	~MenuCalendrier();

	void afficher();
	int enregistrerChoix();
	void choixMenu();
	//void choixCalendrier();
	/*void afficherCalendrierPerso();
	void afficherCalendrierComplet();
	void creerRencontre();*/

private:
	int choix;
};


#endif // !MENUCALENDRIER_H
