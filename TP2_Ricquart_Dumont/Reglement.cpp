#include "Reglement.h"
#include <iostream>

;using namespace std;

Reglement::Reglement()
{

}

Reglement::Reglement(float sseuilEnVigueur, float mmontantTranfert, float mmontantEncaisse, float mmontantRestant, string ddescritpionDesDroits)
{
	seuilEnVigueur=sseuilEnVigueur;
	montantEncaisse=mmontantEncaisse;
	montantRestant=mmontantRestant;
	montantTransfert = mmontantTranfert;
	descriptionDesDroits = ddescritpionDesDroits;
}

Reglement::~Reglement(){}

float Reglement::getSeuilEnVigueur()
{
	return seuilEnVigueur;
}

float Reglement::getMontantTransfert()
{
	return montantTransfert;
}

float Reglement::getMontantEncaisse()
{
	return montantEncaisse;
}

float Reglement::getMontantRestant()
{
	return montantRestant;
}

string Reglement::getDescriptionDesDroits()
{
	return descriptionDesDroits;
}

void Reglement::setSeuilEnVigueur(float sseuilEnVigueur)
{
	seuilEnVigueur = sseuilEnVigueur;
}

void Reglement::setMontantTransfert(float mmontantTransfert)
{
	montantTransfert = mmontantTransfert;
}

void Reglement::setMontantEncaisse(float mmontantEncaisse)
{
	montantEncaisse = mmontantEncaisse;
}

void Reglement::setMontantRestant(float  mmontantRestant)
{
	montantRestant = mmontantRestant;
}

void Reglement::setDescriptionDesDroits(string ddescriptionDesDroits)
{
	descriptionDesDroits = ddescriptionDesDroits;
}

void Reglement::afficher()
{
	cout<<"Seuil en vigueur : "<<seuilEnVigueur<<endl;
	cout<<"Description des droits : "<<descriptionDesDroits<<endl;
	cout<<"Montant du transfert : "<<montantTransfert<<endl;
	cout<<"Montant encaisse : "<<montantEncaisse<<endl;
	cout<<"Montant restant : "<<montantRestant<<endl;
}