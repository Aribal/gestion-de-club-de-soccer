#ifndef PALMARES_H
#define PALMARES_H

#include <iostream>
#include "Club.h"
;using namespace std;

class Palmares
{
public:
	Palmares();
	Palmares(vector<string> ppalmares);
	~Palmares();

	Palmares getPalmares();
	void addPalmares(string nouveauTitre);
	void afficherPalmares();
private:
	vector<string> palmaresClub;
}

#endif // !PALMARES_H
