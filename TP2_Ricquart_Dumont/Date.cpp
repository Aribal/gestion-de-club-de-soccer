#include <string>
#include <iostream>
#include "Date.h"

Date::Date()
{
	jour = mois = annee = 0;
}

Date::Date(int jjour, int mmois, int aannee)
{
	jour = jjour;
	mois = mmois;
	annee = aannee;
}

Date::~Date(){}

int Date::getJour()
{
	return jour;
}

int Date::getMois()
{
	return mois;
}

int Date::getAnnee()
{
	return annee;
}

void Date::setJour(int jjour)
{
	jour = jjour;
}

void Date::setMois(int mmois)
{
	mois = mmois;
}

void Date::setAnnee(int aannee)
{
	annee = aannee;
}
 
void Date::afficher()
{
	cout<<jour<<"/"<<mois<<"/"<<annee<<endl;
}

void Date::creerDate()
{
	cout << "entrez une date (JJ MM AAAA)"<<endl;
	cin >>jour>>mois>>annee;
}

