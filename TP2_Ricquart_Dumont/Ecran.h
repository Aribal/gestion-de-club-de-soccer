#ifndef ECRAN_H
#define ECRAN_H
#include <iostream>
#include <string>
#include <vector>

#include "Ligue.h"
#include "Calendrier.h"
;using namespace std;
class Ecran
{
public:

	int getChoix();
	Ligue getLigue();
	Calendrier getCalendrier();
	void setChoix(int cchoix);
	void setLigue(Ligue lligue);
	void setCalendrier(Calendrier ccalendrier);

	void menuPrincipal();
	void faireMenuPrincipal(int choix);
	void menuClub();
	void faireMenuClub(int choix);
	void menuJoueur();
	void faireMenuJoueur(int choix);
	void menuTransfert();
	void faireMenuTransfert(int choix);
	void menuCalendrier();
	void faireMenuCalendrier(int choix);
	int entrerChoix();
	void effectuerChoix(int cchoix);



private:
	Ligue ligue;
	Calendrier calendrier;
	int choix;

};
#endif // !ECRAN_H
