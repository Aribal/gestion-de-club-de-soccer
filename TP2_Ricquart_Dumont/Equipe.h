#ifndef EQUIPE_H
#define EQUIPE_H

#include <string>
#include "Joueur.h"
#include "Club.h"
#include "Ligue.h"
;using namespace std;

class Equipe:public Club
{
public:
	Equipe();
	Equipe(Club cclub, int nnombreJoueurs, int nnombreGardiens, Joueur ccapitaine);
	~Equipe();

	//Getters
	Club getClub();
	int getNombreJoueurs();
	int getNombreGardiens();
	Joueur getCapitaine();
	string getNomClub();
	//Setters
	void setClub(Club cclub);
	void setNombreJoueurs(int nnombreJoueurs);
	void setNombreGardiens(int nnombreGardiens);
	void setCapitaine(Joueur ccapitaine);
	//M�thodes
	void afficherEquipe();
	void creerEquipe();



private:
	int nombreJoueurs, nombreGardiens;
	Club club;
	Joueur capitaine;
};


#endif // !EQUIPE_H


