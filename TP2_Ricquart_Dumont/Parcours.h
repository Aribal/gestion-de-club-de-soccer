#ifndef PARCOURS_H
#define PARCOURS_H
#include "Date.h"
#include <string>
;using namespace std;

class Parcours
{
public:
	Parcours();
	Parcours(std::string nnomClub, Date ddatePassage);
	~Parcours();

	//Getters
	std::string getNomClub();
	Date getDatePassage();

	void setNomClub(string nnomClub);
	void setDatePassage(Date ddatePassage);

	void afficher();
	void creerParcours();



private:
	std::string nomClub;
	Date datePassage;
};


#endif // !PARCOURS_H
