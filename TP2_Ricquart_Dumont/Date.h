#ifndef DATE_H
#define DATE_H

#include <iostream>

;using namespace std;

class Date
{
public:
	Date();
	Date(int jjour, int mmois, int aannee);
	~Date();

	//GET
	int getJour();
	int getMois();
	int getAnnee();
	
	//SET
	void setJour(int jjour);
	void setMois(int mmois);
	void setAnnee(int aannee);

	//METH
	void afficher();
	void creerDate();

private:
	int jour, mois, annee;
};
#endif // !DATE_H
